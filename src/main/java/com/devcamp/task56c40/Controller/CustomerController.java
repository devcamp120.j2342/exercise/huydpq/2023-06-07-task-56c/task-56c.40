package com.devcamp.task56c40.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c40.Service.CustomerService;
import com.devcamp.task56c40.models.Customer;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")
    public ArrayList<Customer> getCustomer(){
        ArrayList<Customer> customer = customerService.getAllCustomer();
        return customer;
    }
}
