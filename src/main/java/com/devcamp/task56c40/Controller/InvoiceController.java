package com.devcamp.task56c40.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c40.Service.InvoiceService;
import com.devcamp.task56c40.models.Invoice;

@RestController
public class InvoiceController {
    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/invoice")
    public ArrayList<Invoice> getArrayList(){
        ArrayList<Invoice> aInvoices = invoiceService.getAllInvoice();
        return aInvoices;
    }
}
