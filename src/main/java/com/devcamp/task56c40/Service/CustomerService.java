package com.devcamp.task56c40.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task56c40.models.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "huy", 10);
    Customer customer2 = new Customer(2, "lan", 20);
    Customer customer3 = new Customer(3, "hua", 30);

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = new ArrayList<>();
        allCustomer.add(customer1);
        allCustomer.add(customer2);
        allCustomer.add(customer3);
        return allCustomer;
    }
}
