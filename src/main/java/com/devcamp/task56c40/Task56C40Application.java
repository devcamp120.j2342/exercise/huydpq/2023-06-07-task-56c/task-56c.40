package com.devcamp.task56c40;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56C40Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56C40Application.class, args);
	}

}
